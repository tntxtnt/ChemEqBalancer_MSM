#ifndef CHEM_H
#define CHEM_H

#include <algorithm>
#include <string>
#include <map>
#include <set>
#include <cctype>
#include <stack>
#include <boost/msm/front/state_machine_def.hpp>
#include <boost/msm/front/functor_row.hpp>
#include <boost/msm/back/state_machine.hpp>
#include <boost/msm/back/common_types.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/multiprecision/gmp.hpp>
#include <boost/math/common_factor.hpp>

namespace chem
{
namespace msm = boost::msm;
namespace mpl = boost::mpl;
using namespace boost::multiprecision;
using namespace msm::front;
namespace ublas = boost::numeric::ublas;

typedef std::map<std::string,int> ElemsCountMap;
typedef std::vector<std::string> StringVec;
typedef std::set<std::string> StringSet;
typedef std::vector<int> IntVec;
typedef std::vector<ElemsCountMap> EcmVec;
typedef std::pair<StringVec,StringVec> PairOfStringVec;
typedef std::pair<IntVec,IntVec> PairOfIntVec;
typedef std::pair<EcmVec,EcmVec> PairOfEcmVec;
typedef mpq_rational Frac;

// Parentheses checking functions
namespace paren
{
    const std::string p = "()[]";
    bool isopen(char c)
    {
        size_t i = p.find(c);
        return i != p.npos && i%2 == 0;
    }
    bool match(char open, char close)
    {
        size_t i = p.find(close);
        return (i != p.npos && i%2) ? open == p[i-1] : false;
    }
    bool isparen(char c)
    {
        return p.find(c) != p.npos;
    }
}

// Event
struct symb {
    symb(char c) : c(c) {}
    char c;
};
struct flush {};



// Front-end
#define FUNCTOR_DEF(type, name) struct name {\
    template <class EVT,class FSM,class SourceState,class TargetState>\
    type operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt){
#define DUAL_ACT(act1, act2) ActionSequence_< mpl::vector<act1,act2> >

struct FormulaParser_ : public state_machine_def<FormulaParser_>
{
    // list of states
    struct R0 : public state<> {};
    struct R1 : public state<> {};
    struct R2 : public state<> {};
    struct R3 : public state<> {};
    // initial state
    typedef R0 initial_state;
    // list of actions
    FUNCTOR_DEF(void, A1) fsm.elem += evt.c; }};
    FUNCTOR_DEF(void, A2)
        fsm.elemsCount[fsm.elem] += std::max(1, fsm.num);
        fsm.elem.clear();
        fsm.num = 0;
    }};
    FUNCTOR_DEF(void, A3) fsm.num = 10*fsm.num + evt.c - '0'; }};
    FUNCTOR_DEF(void, A4)
        fsm.elemsCountStack.push(fsm.elemsCount);
        fsm.elemsCount.clear();
    }};
    FUNCTOR_DEF(void, A5)
        for (auto& kv : fsm.elemsCount)
            kv.second *= std::max(1, fsm.num);
        fsm.num = 0;
        for (auto const& kv : fsm.elemsCountStack.top())
            fsm.elemsCount[kv.first] += kv.second;
        fsm.elemsCountStack.pop();
    }};
    // list of guards
    FUNCTOR_DEF(bool, G1) return isupper(evt.c); }};
    FUNCTOR_DEF(bool, G2) return islower(evt.c); }};
    FUNCTOR_DEF(bool, G3) return isdigit(evt.c); }};
    FUNCTOR_DEF(bool, G4)
        if (!paren::isopen(evt.c)) return false;
        fsm.par += evt.c;
        return true;
    }};
    FUNCTOR_DEF(bool, G5)
        if (fsm.par.empty()) return false;
        if (!paren::match(fsm.par.back(), evt.c)) return false;
        fsm.par.pop_back();
        return true;
    }};
    // transition table
    struct transition_table : mpl::vector<
        //   Start  Event  Next   Action           Guard
        // +------+------+------+----------------+------+
        Row<  R0  , symb ,  R1  , A1             ,  G1  >,
        Row<  R0  , symb , none , A4             ,  G4  >,
        // +------+------+------+----------------+------+
        Row<  R1  , symb ,  R3  , A2             ,  G5  >,
        Row<  R1  , symb ,  R0  , DUAL_ACT(A2,A4),  G4  >,
        Row<  R1  , symb ,  R2  , A3             ,  G3  >,
        Row<  R1  , symb , none , A1             ,  G2  >,
        Row<  R1  , symb , none , DUAL_ACT(A2,A1),  G1  >,
        Row<  R1  , flush, none , A2                    >,
        // +------+------+------+----------------+------+
        Row<  R2  , symb ,  R3  , A2             ,  G5  >,
        Row<  R2  , symb ,  R0  , DUAL_ACT(A2,A4),  G4  >,
        Row<  R2  , symb ,  R1  , DUAL_ACT(A2,A1),  G1  >,
        Row<  R2  , symb , none , A3             ,  G3  >,
        Row<  R2  , flush, none , A2                    >,
        // +------+------+------+----------------+------+
        Row<  R3  , symb ,  R3  , A5             ,  G5  >,
        Row<  R3  , symb ,  R0  , DUAL_ACT(A5,A4),  G4  >,
        Row<  R3  , symb ,  R1  , DUAL_ACT(A5,A1),  G1  >,
        Row<  R3  , symb , none , A3             ,  G3  >,
        Row<  R3  , flush, none , A5                    >
        // +------+------+------+----------------+------+
    > {};
    // overwrite default no-transition
    template <class FSM,class Event>
    void no_transition(Event const&, FSM&, int) {}
    // list of common variables
    std::stack<ElemsCountMap> elemsCountStack;
    ElemsCountMap elemsCount;
    std::string elem;
    int num = 0;
    std::string par;
};
//////////////////////////////////////////////////////////////////
struct EquationParser_ : public state_machine_def<EquationParser_>
{
    // list of states
    struct R0 : public state<> {};
    // initial state
    typedef R0 initial_state;
    // list of actions
    FUNCTOR_DEF(void, A1) //add c to compound
        fsm.compound += evt.c;
    }};
    FUNCTOR_DEF(void, A2) //add compound to lhs/rhs
        if (fsm.eq > 1) return;
        auto& p = fsm.eq ? fsm.rhs : fsm.lhs;
        p.push_back(fsm.compound);
        fsm.compound.clear();
    }};
    FUNCTOR_DEF(void, A3) fsm.eq++; }};
    // list of guards
    FUNCTOR_DEF(bool,G1) return isalnum(evt.c) || paren::isparen(evt.c); }};
    FUNCTOR_DEF(bool,G2) return evt.c == '+' && !fsm.compound.empty(); }};
    FUNCTOR_DEF(bool,G3) return evt.c == '=' && !fsm.compound.empty(); }};
    FUNCTOR_DEF(bool,G4) return isspace(evt.c); }};
    // transition table
    struct transition_table : mpl::vector<
        //   Start  Event  Next   Action           Guard
        // +------+------+------+----------------+------+
        Row<  R0  , symb , none , none           ,  G4  >, // spaces
        Row<  R0  , symb , none , DUAL_ACT(A2,A3),  G3  >, // =
        Row<  R0  , symb , none , A2             ,  G2  >, // +
        Row<  R0  , symb , none , A1             ,  G1  >, // alnum/parentheses
        Row<  R0  , flush, none , A2                    >
        // +------+------+------+----------------+------+
    > {};
    // list of common variables
    std::vector<std::string> lhs, rhs;
    std::string compound;
    int eq = 0;
};
#undef FUNCTOR_DEF
#undef DUAL_ACT



// Back-end
namespace formula
{
    ElemsCountMap parse(const std::string& s)
    {
        using msm::back::HANDLED_TRUE;
        msm::back::state_machine<FormulaParser_> p;
        bool good = true;
        for (char c : s) if (p.process_event(symb(c)) != HANDLED_TRUE)
        { good = false; break; }
        if (good) good = p.process_event(flush()) == HANDLED_TRUE;
        return good && p.par.empty() ? p.elemsCount : ElemsCountMap();
    }
}
namespace equation
{
    PairOfStringVec parse(const std::string& eq)
    {
        using msm::back::HANDLED_TRUE;
        msm::back::state_machine<EquationParser_> p;
        bool good = true;
        for (char c : eq) if (p.process_event(symb(c)) != HANDLED_TRUE)
        { good = false; break; }
        if (good) good = p.process_event(flush()) == HANDLED_TRUE;
        if (!good || p.eq != 1) return PairOfStringVec();
        return {p.lhs, p.rhs};
    }
    PairOfEcmVec parseEcm(const PairOfStringVec& ss)
    {
        PairOfEcmVec ret;
        for (auto& s : ss.first)  ret.first.push_back(formula::parse(s));
        for (auto& s : ss.second) ret.second.push_back(formula::parse(s));
        return ret;
    }
    StringSet elements(const EcmVec& ev)
    {
        StringSet ret;
        for (auto& ecm : ev) for (auto& kv : ecm) ret.insert(kv.first);
        return ret;
    }
    ublas::vector<Frac>& toWholeNumbers(ublas::vector<Frac>& B)
    {
        auto lcd = std::accumulate(begin(B), end(B), denominator(B(0)),
            [](const mpz_int& a, const Frac& b){
                return boost::math::lcm(a, denominator(b)); });
        std::transform(begin(B), end(B), begin(B),
            [&lcd](const Frac& f){ return f*lcd; });
        return B;
    }
    PairOfIntVec splitToInts(const ublas::vector<Frac>& B, int i)
    {
        PairOfIntVec ret;
        auto f2i = [](const Frac& f){ return (int)numerator(f); };
        std::transform(begin(B), begin(B)+i, back_inserter(ret.first), f2i);
        std::transform(begin(B)+i, end(B), back_inserter(ret.second), f2i);
        return ret;
    }
    PairOfIntVec coefRE1(PairOfEcmVec& ee, const StringSet& elems, int E)
    {
        // create ExE matrix
        ublas::matrix<Frac> A(E, E);
        ublas::vector<Frac> B(E);
        int i = 0;
        for (auto& elem : elems)
        {
            int j = 0;
            for (auto& compound : ee.first)
                A(i,j++) = compound[elem];
            for (size_t k = 0; k < ee.second.size()-1; ++k)
                A(i,j+k) = -ee.second[k][elem];
            B(i++) = ee.second.back()[elem];
        }
        // solve AX = B
        ublas::permutation_matrix<size_t> pm(A.size1());
        if (lu_factorize(A, pm) > 0) return PairOfIntVec(); //singular matrix
        lu_substitute(A, pm, B);
        for (i = 0; i < E; ++i) if (B(i)==0) return PairOfIntVec(); //zero coef
        // convert x_i to whole number and split X to lhs&rhs
        ublas::vector<Frac> X(B.size()+1);
        std::copy(begin(B), end(B), begin(X)); X(B.size()) = 1;
        return splitToInts(toWholeNumbers(X), ee.first.size());
    }
    PairOfIntVec coefRE0(PairOfEcmVec& ee, const StringSet& elems, int E, int R)
    {
        // create ExR matrix
        ublas::matrix<Frac> A(E, R);
        int i = 0;
        for (auto& elem : elems)
        {
            int j = 0;
            for (auto& compound : ee.first)
                A(i,j++) = compound[elem];
            for (size_t k = 0; k < ee.second.size(); ++k)
                A(i,j+k) = -ee.second[k][elem];
            ++i;
        }
        // LU factorize
        ublas::permutation_matrix<size_t> pm(A.size1());
        if (lu_factorize(A, pm) != R) return PairOfIntVec();
        // submatrix, subvector
        ublas::matrix<Frac> M = ublas::subrange(A, 0,R-1, 0,R-1);
        ublas::vector<Frac> B(R-1);
        for (int i = R-1; i--; B(i) = -A(i,R-1));
        // backward substitution
        inplace_solve(M, B, ublas::upper_tag());
        for (i = 0; i < R-1; ++i) if (B(i) == 0) return PairOfIntVec();
        // convert x_i to whole number and split X to lhs&rhs
        ublas::vector<Frac> X(B.size()+1);
        std::copy(begin(B), end(B), begin(X)); X(B.size()) = 1;
        return splitToInts(toWholeNumbers(X), ee.first.size());
    }
    PairOfIntVec coefficients(const PairOfStringVec& ss)
    {
        // check valid elements in both sides
        PairOfEcmVec ee = parseEcm(ss);
        auto elementsSet = elements(ee.first);
        if (elements(ee.second) != elementsSet) return PairOfIntVec();
        // check R - E
        int R = ee.first.size() + ee.second.size();
        int E = elementsSet.size();
        if (R - E > 1) return PairOfIntVec();
        PairOfIntVec ret;
        if (R - E == 1) return coefRE1(ee, elementsSet, E);
        return coefRE0(ee, elementsSet, E, R);
    }
}
} //chem

#endif // CHEM_H
