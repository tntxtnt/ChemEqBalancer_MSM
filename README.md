## Chemical Equation Balancer 

[![build status](https://gitlab.com/tntxtnt/ChemEqBalancer_MSM/badges/master/build.svg)](https://gitlab.com/tntxtnt/ChemEqBalancer_MSM/commits/master)

#### Summary
Use Boost.MSM to parse chemical formula (overkill I know) and Boost.UBLAS lu_factorize to solve the coeficients linear system.

Example input:
```
std::vector<std::string> testInputs = {
    "Fe + H2SO4 = Fe2(SO4)3 + SO2 + H2O",
    "K4Fe(CN)6 + KMnO4 + H2SO4 = KHSO4 + Fe2(SO4)3 + MnSO4 + HNO3 + CO2 + H2O",
    "K4[Fe(SCN)6] + K2Cr2O7 + H2SO4 = Fe2(SO4)3 + Cr2(SO4)3 + CO2 + H2O + K2SO4 + KNO3",
    "H2O + CO2 = H2CO3",
    "Fe2O3 + HCl = FeCl3 + H2O",
    "K2Cr2O7 + H2SO3 + HCl = KCl + Cr2(SO4)3 + H2O",
    "K2Cr2O7 + H2SO3 + H2SO4 = K2SO4 + Cr2(SO4)3 + H2O",
    "NaCl + H2O = NaCl(H2O)6",
    "NaClCaKPU + H2O = NaCl(H2O)6CaKPU",

    "Fe + H2SO4 = Fe2(SO4)3 + SO3 + H2O", //SO2 -> SO3
    "K2Cr2O7 + H2SO3 + HCl = K2SO4 + Cr2(SO4)3 + H2O", //no Cl on rhs
    "NaClCaKPU + H2O = NaCl(H2O)6CaKPU2", //U2
};
```

Example output:
```
2Fe + 6H2SO4 = Fe2(SO4)3 + 3SO2 + 6H2O
10K4Fe(CN)6 + 122KMnO4 + 299H2SO4 = 162KHSO4 + 5Fe2(SO4)3 + 122MnSO4 + 60HNO3 + 60CO2 + 188H2O
6K4[Fe(SCN)6] + 97K2Cr2O7 + 355H2SO4 = 3Fe2(SO4)3 + 97Cr2(SO4)3 + 36CO2 + 355H2O + 91K2SO4 + 36KNO3
H2O + CO2 = H2CO3
Fe2O3 + 6HCl = 2FeCl3 + 3H2O
K2Cr2O7 + 3H2SO3 + 2HCl = 2KCl + Cr2(SO4)3 + 4H2O
K2Cr2O7 + 3H2SO3 + H2SO4 = K2SO4 + Cr2(SO4)3 + 4H2O
NaCl + 6H2O = NaCl(H2O)6
NaClCaKPU + 6H2O = NaCl(H2O)6CaKPU
No solution or no unique solution (Fe + H2SO4 = Fe2(SO4)3 + SO3 + H2O)
No solution or no unique solution (K2Cr2O7 + H2SO3 + HCl = K2SO4 + Cr2(SO4)3 + H2O)
No solution or no unique solution (NaClCaKPU + H2O = NaCl(H2O)6CaKPU2)
```

#### Dependencies
- Boost.MSM
- Boost.UBLAS
- Boost.Multiprecision
- Boost.Math

#### Why
- Have fun exploring Boost.MSM
- Get Boost.UBLAS to work with cpp_int (-DNDEBUG)
- And to automatically balance chemical equations of course!

#### Links
- [Part 1 (Vietnamese)](https://tritran.xyz/posts/can-bang-phan-ung-hoa-hoc/)
- [Part 2 (Vietnamese)](https://tritran.xyz/posts/can-bang-phan-ung-hoa-hoc-2/)
