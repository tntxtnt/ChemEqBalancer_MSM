// Copyright 2016 Tri Tran
// Distributed under MIT license
//
// Chemical equation solver
//
#include <iostream>
#include "chem.h"

int main()
{
    std::vector<std::string> testInputs = {
        "Fe + H2SO4 = Fe2(SO4)3 + SO2 + H2O",
        "K4Fe(CN)6 + KMnO4 + H2SO4 = KHSO4 + Fe2(SO4)3 + MnSO4 + HNO3 + CO2 + H2O",
        "K4[Fe(SCN)6] + K2Cr2O7 + H2SO4 = Fe2(SO4)3 + Cr2(SO4)3 + CO2 + H2O + K2SO4 + KNO3",
        "H2O + CO2 = H2CO3",
        "Fe2O3 + HCl = FeCl3 + H2O",
        "K2Cr2O7 + H2SO3 + HCl = KCl + Cr2(SO4)3 + H2O",
        "K2Cr2O7 + H2SO3 + H2SO4 = K2SO4 + Cr2(SO4)3 + H2O",
        "NaCl + H2O = NaCl(H2O)6",
        "NaClCaKPU + H2O = NaCl(H2O)6CaKPU",

        "Fe + H2SO4 = Fe2(SO4)3 + SO3 + H2O", //SO2 -> SO3
        "K2Cr2O7 + H2SO3 + HCl = K2SO4 + Cr2(SO4)3 + H2O", //no Cl on rhs
        "NaClCaKPU + H2O = NaCl(H2O)6CaKPU2", //U2
    };

    for (auto& eq : testInputs)
    {
        auto ss = chem::equation::parse(eq);
        if (ss.first.empty())
        {
            std::cout << "Invalid input equation (" << eq << ")\n";
            continue;
        }
        auto cc = chem::equation::coefficients(ss);
        if (cc.first.empty())
        {
            std::cout << "No solution or no unique solution (" << eq << ")\n";
            continue;
        }
        // print solution
        for (size_t i = 0; i < ss.first.size(); ++i)
        {
            if (cc.first[i] != 1) std::cout << cc.first[i];
            std::cout << ss.first[i] << (i < ss.first.size()-1?" + ":" = ");
        }
        for (size_t i = 0; i < ss.second.size(); ++i)
        {
            if (cc.second[i] != 1) std::cout << cc.second[i];
            std::cout << ss.second[i] << (i < ss.second.size()-1?" + ":"\n");
        }
    }
}
